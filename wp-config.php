<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'a');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'lk3q90J5qe*h:w9`-wH~FzL+p:9>gJ-$BBBJ-uue NJhjhYuL+f3Y*<Q-/HT 1++');
define('SECURE_AUTH_KEY',  'MN3VB:>Uaad22x#!hmE+-Hmd4c`t=#%On6Y^M6^|Fvxt<hb}|feH)GF^_PuoJmlu');
define('LOGGED_IN_KEY',    'm|E?2r- },j]v8LvZ-I6?W`EC=K@>a_6g:S>4[fFJ:):T-><vr#ez$qX.J:-Z%Bk');
define('NONCE_KEY',        'tuIAy@^<gQJ<5_j|zLFe(tl1;-tPo[[ 0p7III5QHA{3R77_e60LI;_X.2`W~@lh');
define('AUTH_SALT',        'Yo8Tl+}*}7|!L#Rj<7}rytXYEr+)h+);H<6.kq],|yJfv*4C#Qq?2Kc%QyRc1eGR');
define('SECURE_AUTH_SALT', 'S{ONd{OL@+*lQIu=G^:M{Y%T+@ke2S{AkniIA(VL?BGM$TD[usq~N.E@a3w6g=De');
define('LOGGED_IN_SALT',   'Bg(wY5@6*HCh3}z$_bH,D9;2}/]GT/5V{x bYbNog?N1nplssqxR!;OKn4u7;J}I');
define('NONCE_SALT',       '<+oh-=:+ldA6>4J0|w!h-#qp|i)Tum6TIf.qi0V[A{]:pQ%:}`{WcfyI!fmQ[>Q$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
define('FS_METHOD', 'direct');